'''
    Descripcion:    Se quiere hacer un programa en Python para jugar el “Ahorcado 3000Plus”. Este juego consiste
                    en que el usuario indica una palabra secreta y luego se trata de adivinar esa palabra letra 
                    por letra, hasta que se adivine la pablara o se tengan máximo 7 intentos fallidos. La palabra 
                    secreta debe cumplir con las siguientes condiciones:
                        1.  Debe tener más de 7 caracteres.
                        2.  No debe iniciar con vocal en minúscula.
                        3.  La cantidad de vocales de la palabra debe ser inferior a la cantidad de consonantes de la palabra.
                    Si la palabra secreta no es válida, el programa debe solicitar otra palabra secreta hasta que cumpla
                    con los criterios establecidos. Cuando la palabra secreta es válida se debe iniciar el juego. Al 
                    iniciar el juego se debe imprimir en pantalla una bienvenida al juego y la horca inicial. Luego 
                    el programa lee letra por letra del usuario hasta que el usuario tenga máximo 7 intentos fallidos 
                    o hasta que adivine la palabra completa con todas sus letras. Cada vez que se lee una letra del 
                    usuario se debe verificar si la letra se encuentra en la palabra secreta, si no se encuentra se 
                    debe imprimir la horca y con cada intento fallido se va avanzando hasta dibujar completamente el 
                    ahorcado, es decir, hasta que tenga máximo 7 intentos fallidos. Si la letra sí se encuentra en la 
                    palabra secreta, se imprime la palabra secreta con todas las letras adivinadas hasta el momento y 
                    para las letras que aún no se adivinan se imprime un “_” (guiónbajo o unserscore). Ver ejemplo al 
                    final del enunciado.
    Programador: Michelle Paniagua 
    Fecha de creacion: 24 abril, 2022
    Version: 1.0
'''
 

# Rutina que lee la palabra
from pprint import pp
import os

def leerPalabra():
    try:
        pPalabra = input("Escriba la palabra secreta: ")
        esPalabraValida(pPalabra)
    except ValueError:
        print("Valor invalido ")
        leerPalabra() 

# Rutina para validar la palabra 
def esPalabraValida(pPalabra):
    cantLetras = len(pPalabra)  
    priLetra = pPalabra[0]
    
    if cantLetras >= 7: 
        if priLetra != 'a' and priLetra != 'e' and priLetra != 'i' and priLetra != 'o' and priLetra != 'u' : 
            vocales = contarVocales(pPalabra) 
            consonantes = len(pPalabra) - vocales 
            if  vocales <= consonantes:
                print("Palabra valida")
                imprimirBienvenida(pPalabra)
            else:
                print("No es valida la palabra secreta ")
                leerPalabra()
        else:
                print("No es valida la palabra secreta ")
                leerPalabra()
    
    else:
        print("No es valida la palabra secreta ")
        leerPalabra()

# Rutina que cuenta las vocales   
def contarVocales(pPalabra):
    vocales = 0
    for c in pPalabra:
        if c in "aeiou": 
            vocales = vocales + 1
    return vocales  

# Rutina para imprimir bienvenida
def imprimirBienvenida(pPalabra):
    guion = ['_'] * len(pPalabra)
     
    print("------------------------------")
    print("Bienvenido al Ahorcado 300Plus ")
    print("------------------------------\n")

    print("Palabra:", end="") 
    for i in guion:
        print(i, end=' ')

    print()
    imprimirHorca() 
    juego(pPalabra, guion)


# Rutina Imprimir Horca
def imprimirHorca():   
    print("\t+---+")
    print("\t|   |")
    print("\t|    ")
    print("\t|    ")
    print("\t|    ")
    print("\t|    ")
    print("\t=========")
 
# Rutina Pierde 1 intento
def imprimirPierde1Intento():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|    ")
    print("\t|    ")
    print("\t|    ")
    print("\t=========")

# Rutina Pierde 2 intentos
def imprimirPierde2Intentos():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|   |")
    print("\t|    ")
    print("\t|    ")
    print("\t=========")

# Rutina Pierde 3 intentos
def imprimirPierde3Intentos():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|  /|")
    print("\t|    ")
    print("\t|    ")
    print("\t=========")

# Rutina Pierde 4 intentos
def imprimirPierde4Intentos():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|  /|\ ")
    print("\t|    ")
    print("\t|    ")
    print("\t=========")

# Rutina Pierde 5 intentos
def imprimirPierde5Intentos():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|  /|\ ")
    print("\t|   |")
    print("\t|    ")
    print("\t=========")

# Rutina Pierde 6 intentos
def imprimirPierde6Intentos():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|  /|\ ")
    print("\t|   |")
    print("\t|  /   ")
    print("\t=========")

# Rutina Pierde 7 intentos
def imprimirPierde7Intentos():   
    print("\t+---+")
    print("\t|   |")
    print("\t|   O")
    print("\t|  /|\ ")
    print("\t|   |")
    print("\t|  / \ ")
    print("\t=========")

# Rutina Gana el Juego 
def imprimirGanaJuego():
    print("---------------------------------")
    print("Excelente, ha ganado el juego!!")
    print("---------------------------------")

# Rutina Pierde el Juego 
def imprimirPierdeJuego():
    print("---------------------------------")
    print("Lo sentimos, ha perdido el juego!!")
    print("---------------------------------")

# Rutina del juego
def juego(pPalabra, pGuion ):
    jugar = "s"
    intento = 1 
    letraCorrectas = 0
    while jugar == "s":
        letraJugada = input("Por favor escriba una letra: ")
        
        posicion = pPalabra.find(letraJugada) 
        

        if posicion == -1:
            if intento == 1: 
                os.system('cls')
                imprimirPierde1Intento()
            elif intento == 2:
                os.system('cls')
                imprimirPierde2Intentos()
            elif intento == 3:
                os.system('cls')
                imprimirPierde3Intentos()
            elif intento == 4:
                os.system('cls')
                imprimirPierde4Intentos()
            elif intento == 5:
                os.system('cls')
                imprimirPierde5Intentos()
            elif intento == 6:
                os.system('cls')
                imprimirPierde6Intentos()
            else:
                os.system('cls')
                imprimirPierde7Intentos()
                imprimirPierdeJuego()
                jugar = "n"
            intento +=1


        else:
            pGuion[posicion] = letraJugada
            for i in pGuion: 
                print(i, end=' ')

            print(" ")
            letraCorrectas +=1
            print(letraCorrectas)
            if letraCorrectas == len(pPalabra):
                imprimirGanaJuego()
                jugar = "n"
         

# Main
leerPalabra() 

